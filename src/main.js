import Vue from 'vue'
import App from './App.vue'
import VueRouter from '../node_modules/vue-router'
import CheckEmail from './components/CheckEmail/CheckEmail.vue'
import SourceList from './components/SourceList/SourceList.vue'

Vue.use(VueRouter);

Vue.config.productionTip = false;

const routes = [
  { path: '/', component: CheckEmail },
  { path: '/sources', component: SourceList }
];

const router = new VueRouter({
  routes
});

const app = new Vue({
  router,
  render: h => h(App)
}).$mount('#app');

