<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 31.01.2019
 * Time: 18:14
 */
header("Access-Control-Allow-Origin: *");

isset($_GET['key']) ? $key = $_GET['key'] : NULL;
isset($_GET['source']) ? $source = $_GET['source'] : NULL;

$data = array(
    "sergey.kul8@gmail.com" => array(
        "000webhost.com" => array(
            "Name" => "000webhost",
            "Title" => "000webhost",
            "Domain" => "000webhost.com",
            "BreachDate" => "2015-03-01",
            "AddedDate" => "2015-10-26T23:35:45Z",
            "ModifiedDate" => "2017-12-10T21:44:27Z",
            "PwnCount" => 14936670,
            "Description" => "In approximately March 2015, the free web hosting provider <a href=\"http://www.troyhunt.com/2015/10/breaches-traders-plain-text-passwords.html\" target=\"_blank\" rel=\"noopener\">000webhost suffered a major data breach</a> that exposed almost 15 million customer records. The data was sold and traded before 000webhost was alerted in October. The breach included names, email addresses and plain text passwords.",
            "LogoPath" => "https://haveibeenpwned.com/Content/Images/PwnedLogos/000webhost.png",
            "DataClasses" => array(
                "Email addresses","IP addresses","Names","Passwords"
            ),
            "IsVerified" => true,
            "IsFabricated" => false,
            "IsSensitive" => false,
            "IsRetired" => false,
            "IsSpamList" => false
        ),
        "vk.com" => array(
            "Name" => "VK",
            "Title" => "VK",
            "Domain" => "vk.com",
            "BreachDate" => "2012-01-01",
            "AddedDate" => "2016-06-09T09:16:36Z",
            "ModifiedDate" => "2016-06-09T09:16:36Z",
            "PwnCount" => 93338602,
            "Description" => "In approximately 2012, the Russian social media site known as <a href=\"http://motherboard.vice.com/read/another-day-another-hack-100-million-accounts-for-vk-russias-facebook\" target=\"_blank\" rel=\"noopener\">VK was hacked</a> and almost 100 million accounts were exposed. The data emerged in June 2016 where it was being sold via a dark market website and included names, phone numbers email addresses and plain text passwords.",
            "LogoPath" => "https://haveibeenpwned.com/Content/Images/PwnedLogos/VK.png",
            "DataClasses" => array(
                "Email addresses","Names","Passwords","Phone numbers"
            ),
            "IsVerified" => true,
            "IsFabricated" => false,
            "IsSensitive" => false,
            "IsRetired" => false,
            "IsSpamList" => false
        ),
		"parapa.mail.ru" => array(
            "Name" => "Parapa",
            "Title" => "Пара Па",
            "Domain" => "parapa.mail.ru",
            "BreachDate" => "2016-08-08",
            "AddedDate" => "2016-12-28T07:03:17Z",
            "ModifiedDate" => "2016-12-28T07:03:17Z",
            "PwnCount" => 4946850,
            "Description" => "In August 2016, <a href=\"http://www.zdnet.com/article/over-25-million-accounts-stolen-after-mail-ru-forums-raided-by-hackers/\" target=\"_blank\" rel=\"noopener\">the Russian gaming site known as Пара Па (or parapa.mail.ru) was hacked</a> along with a number of other forums on the Russian mail provider, mail.ru. The vBulletin forum contained 4.9 million accounts including usernames, email addresses and passwords stored as salted MD5 hashes.",
            "LogoPath" => "https://haveibeenpwned.com/Content/Images/PwnedLogos/Parapa.png",
            "DataClasses" => array(
                "Email addresses", "Passwords", "Usernames"
            ),
            "IsVerified" => true,
            "IsFabricated" => false,
            "IsSensitive" => false,
            "IsRetired" => false,
            "IsSpamList" => false
        ),
		"cfire.mail.ru" => array(
            "Name" => "Crossfire",
            "Title" => "Crossfire",
            "Domain" => "cfire.mail.ru",
            "BreachDate" => "2016-08-08",
            "AddedDate" => "2016-12-28T00:29:28Z",
            "ModifiedDate" => "2016-12-28T00:29:28Z",
            "PwnCount" => 12865609,
            "Description" => "In August 2016, <a href=\"http://www.zdnet.com/article/over-25-million-accounts-stolen-after-mail-ru-forums-raided-by-hackers/\" target=\"_blank\" rel=\"noopener\">the Russian gaming forum known as Cross Fire (or cfire.mail.ru) was hacked</a> along with a number of other forums on the Russian mail provider, mail.ru. The vBulletin forum contained 12.8 million accounts including usernames, email addresses and passwords stored as salted MD5 hashes.",
            "LogoPath" => "https://haveibeenpwned.com/Content/Images/PwnedLogos/CrossFire.png",
            "DataClasses" => array(
                "Email addresses", "Passwords", "Usernames"
            ),
            "IsVerified" => true,
            "IsFabricated" => false,
            "IsSensitive" => false,
            "IsRetired" => false,
            "IsSpamList" => false
        )
    ),
    "test@example.com" => array(
        "vk.com" => array(
            "Name" => "VK",
            "Title" => "VK",
            "Domain" => "vk.com",
            "BreachDate" => "2012-01-01",
            "AddedDate" => "2016-06-09T09:16:36Z",
            "ModifiedDate" => "2016-06-09T09:16:36Z",
            "PwnCount" => 93338602,
            "Description" => "In approximately 2012, the Russian social media site known as <a href=\"http://motherboard.vice.com/read/another-day-another-hack-100-million-accounts-for-vk-russias-facebook\" target=\"_blank\" rel=\"noopener\">VK was hacked</a> and almost 100 million accounts were exposed. The data emerged in June 2016 where it was being sold via a dark market website and included names, phone numbers email addresses and plain text passwords.",
            "LogoPath" => "https://haveibeenpwned.com/Content/Images/PwnedLogos/VK.png",
            "DataClasses" => array(
                "Email addresses","Names","Passwords","Phone numbers"
            ),
            "IsVerified" => true,
            "IsFabricated" => false,
            "IsSensitive" => false,
            "IsRetired" => false,
            "IsSpamList" => false
        )
    )
);

if(array_key_exists($key, $data))
{
    if(isset($source))
    {
        if(array_key_exists($source, $data[$key]))
        {
            echo json_encode(array($data[$key][$source]));
        }
        else
        {
            echo json_encode(array());
        }
    }
    else
    {
        echo json_encode($data[$key]);
    }
}
else
{
    echo json_encode(array());
}

?>